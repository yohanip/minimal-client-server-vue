// @flow

const Express = require('express')
const bodyParser = require('body-parser')
const port = 19999

import type {
  $Request,
  $Response
} from 'express'

const app = Express()

app.use(bodyParser.json())

type FunctionResult = {
  result ? : any,
  error ? : any
}

async function insertData(collection: string, object: any): Promise < FunctionResult > {
  try {
    const result = true
    return {
      result
    }
  } catch (error) {
    return {
      error
    }
  }
}

app.get('/api', (req: $Request, res: $Response) => {
  res.send('ok')
})

app.post('/api', (req: $Request, res: $Response) => {
  res.send('OK POST')
})



app.listen(port, '127.0.0.1', function () {
  console.log('server listening at port', port)
})