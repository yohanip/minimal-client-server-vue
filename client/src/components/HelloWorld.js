// @flow
import Axios from 'axios'

export default {
  data() {
    return {
      loading: false,
      content: 'NOT YET FETCHED'
    }
  },
  async mounted() {

  },
  methods: {
    async loadContent() {
      let r = await Axios.get('/api')
      let response = r.data
      this.content = response
    }
  }
}
